﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Linq;
using Util.Logging;
using FilesystemManager.Enums;
using System.Diagnostics;

namespace FilesystemManager.FileManager
{
    public class FileManagerService
    {
        private readonly FileInfo fileInfo;
        private readonly DirectoryInfo directoryInfo;
        private LoggingService logger;
        private readonly Stopwatch sw = new Stopwatch();
        public FileManagerService(string directory)
        {
            // Checks if the directory exists or not
            try
            {
                directoryInfo = new DirectoryInfo(Directory.GetCurrentDirectory() + directory);
                fileInfo = new FileInfo(Directory.GetCurrentDirectory() + directory + @"/dracula.txt");
                if (!fileInfo.Exists || !directoryInfo.Exists)
                {
                    throw new IOException($"Could not find directory '{directory}'");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            try
            {
                string path = Directory.GetCurrentDirectory() + @"/logs/filemaniplog.txt";
                logger = new LoggingService(path);
            }
            catch (IOException ex)
            {
                throw ex;
            }
        }

        public string[] ListFiles()
        {
            sw.Start();
            string[] files = directoryInfo.GetFiles().Select(file => file.Name.Split("/")[^1]).ToArray();
            sw.Stop();

            logger.LogFileManip(FileOperations.LIST_FILES, sw.ElapsedMilliseconds);

            sw.Reset();
            return files;
        }

        public string[] ListFileTypes()
        {
            sw.Start();
            FileInfo[] files = directoryInfo.GetFiles();
            HashSet<string> fileExtentions = new HashSet<string>();

            foreach (FileInfo file in files)
            {
                string fileExtention = file.Extension;
                fileExtentions.Add(fileExtention[1..]);
            }
            sw.Stop();

            logger.LogFileManip(FileOperations.LIST_FILE_EXT, sw.ElapsedMilliseconds);

            sw.Reset();
            return fileExtentions.ToArray();
        }

        public string[] getFilesByExt(string ext)
        {
            sw.Start();
            string[] files = directoryInfo.GetFiles().Where(file => file.Extension.Contains($"{ext}")).Select(file => file.Name).ToArray();
            sw.Stop();

            logger.LogFileManip(FileOperations.GET_FILE_BY_EXT, sw.ElapsedMilliseconds);

            sw.Reset();
            return files;
        }

        public string FileName()
        {
            sw.Start();
            string fileName = fileInfo.Name;
            sw.Stop();

            logger.LogFileManip(FileOperations.FILE_NAME, sw.ElapsedMilliseconds);

            sw.Reset();
            return fileName;
        }

        public string FileSize()
        {
            sw.Start();
            string fileSize = fileInfo.Length.ToString();
            sw.Stop();

            logger.LogFileManip(FileOperations.FILE_SIZE, sw.ElapsedMilliseconds);

            sw.Reset();
            return fileSize;
        }

        public string FileLineLength()
        {
            sw.Start();
            string fileLineLength = File.ReadLines(fileInfo.FullName).Count().ToString();
            sw.Stop();

            logger.LogFileManip(FileOperations.COUNT_LINES, sw.ElapsedMilliseconds);

            sw.Reset();
            return fileLineLength;
        }

        public (string word, int count) FindAndCountWord(string word)
        {
            sw.Start();
            string lowerCasedWord = word.ToLower();
            // word match must be exact. Examples: "dracula" != dracula || it's != its etc..
            int wordCount = File.ReadLines(fileInfo.FullName)
                                .Where(line => line.Split(' ').Where(word => word.ToLower() == lowerCasedWord).Count() > 0)
                                .Count();
            sw.Stop();
            logger.LogSearchTerm(word, wordCount, sw.ElapsedMilliseconds);

            sw.Reset();
            return ($"{word}", wordCount);
        }
    }
}
