﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using FilesystemManager.Enums;

namespace Util.Logging
{
    public class LoggingService
    {
        private readonly FileInfo logFile;
        // Creates the logs directory if it doesn't exist
        public LoggingService(string logFilePath)
        {
            logFile = new FileInfo(logFilePath);
            if (!logFile.Exists)
            {
                try
                {
                    logFile.Directory.Create();
                }
                catch (Exception ex)
                {
                    throw new IOException(String.Format($"Could not create the log file with path {logFilePath}"));
                }
            }
        }
        public void LogFileManip(FileOperations operation, double delta)
        {
            switch (operation)
            {
                case FileOperations.COUNT_LINES:
                    WriteToFile(CreateLogLine("Counted line count", delta));
                    break;
                case FileOperations.FILE_NAME:
                    WriteToFile(CreateLogLine("Got the file name", delta));
                    break;
                case FileOperations.FILE_SIZE:
                    WriteToFile(CreateLogLine("Got file size", delta));
                    break;
                case FileOperations.LIST_FILES:
                    WriteToFile(CreateLogLine("Listed files in the directory", delta));
                    break;
                case FileOperations.LIST_FILE_EXT:
                    WriteToFile(CreateLogLine("Listed all the file extension in the directory", delta));
                    break;
                case FileOperations.GET_FILE_BY_EXT:
                    WriteToFile(CreateLogLine("Got files by extension", delta));
                    break;
                default:
                    WriteToFile(CreateLogLine("Unknown file operation called", delta));
                    break;
            }
        }

        public void LogSearchTerm(string searchTerm, int results, double delta)
        {
            this.WriteToFile(CreateLogLine($"The term '{searchTerm}' was found {results} time(s)", delta));
        }

        private void WriteToFile(string line)
        {
            try
            {
                using (StreamWriter sw = logFile.AppendText())
                {
                    sw.WriteLine(line);
                }
            }
            catch (IOException ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
        }

        private static string CreateLogLine(string message, double delta)
        {
            DateTime now = DateTime.Now;
            return $"{now}: {message}. The function took {delta}ms to execute";
        }
    }
}
