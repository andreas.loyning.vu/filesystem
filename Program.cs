﻿using System;
using Util.Logging;
using System.IO;
using System.Collections.Generic;
using FilesystemManager.FileManager;


namespace FilesystemManager
{
    class Program
    {
        static void Main(string[] args)
        {
            Promt();
        }

        // The main promt function.
        static void Promt()
        {
            Console.CursorVisible = false;
            int consoleTop = Console.CursorTop;
            FileManagerService fileManager = null;
            try
            {
                fileManager = new FileManagerService(@"/resources");
            }
            catch (IOException ex)
            {
                Console.WriteLine($"Error: {ex.Message}. Exiting.");
                Environment.Exit(1);
            }
            bool active = true;
            // Default position of the cursor
            int selectedOption = 2;
            while (active)
            {
                clearConsole(0, consoleTop);
                Console.Write(
                    $"Please select one of the options\n" +
                    $"{(selectedOption == 2 ? '>' : (' '))}  List all file names in the resources directory\n" +
                    $"{(selectedOption == 1 ? '>' : (' '))}  Show files by extension\n" +
                    $"{(selectedOption == 0 ? '>' : (' '))}  Manilpulate the dracula file (Will display a new menu)\n" +
                    $"Use arrow keys to select. Escape to exit"
                    );
                consoleTop = Console.CursorTop - 4;

                ConsoleKeyInfo userInput = Console.ReadKey();
                // Listening for user input
                while (userInput.Key != ConsoleKey.Enter)
                {
                    if (userInput.Key == ConsoleKey.UpArrow)
                    {
                        if (selectedOption >= 0 && selectedOption < 2)
                        {
                            selectedOption++;
                        }
                    }
                    else if (userInput.Key == ConsoleKey.DownArrow)
                    {
                        if (selectedOption > 0 && selectedOption <= 2)
                        {
                            selectedOption--;
                        }
                    }
                    else if (userInput.Key == ConsoleKey.Escape)
                    {
                        Environment.Exit(1);
                        break;
                    }
                    clearConsole(0, consoleTop);
                    Console.Write(
                                $"Please select one of the options\n" +
                                $"{(selectedOption == 2 ? '>' : (' '))}  List all file names in the resources directory\n" +
                                $"{(selectedOption == 1 ? '>' : (' '))}  Show files by extension\n" +
                                $"{(selectedOption == 0 ? '>' : (' '))}  Manilpulate the dracula file (Will display a new menu)"
                        );
                    // Need to this, shows duplicate stuff when at bottom of Console
                    consoleTop = Console.CursorTop - 3;
                    userInput = Console.ReadKey();
                }

                // Clear the lines written
                clearConsole(0, consoleTop);
                Console.ForegroundColor = ConsoleColor.Cyan;
                // Invoking function based on the selected option
                switch (selectedOption)
                {
                    case 2:
                        Console.WriteLine("The resources directory contains these files:");
                        string[] files = fileManager.ListFiles();
                        foreach (string file in files)
                        {
                            Console.WriteLine(file);
                        }
                        break;
                    case 1:
                        fileExtensionPromt(fileManager);
                        consoleTop = Console.CursorTop;
                        break;
                    case 0:
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.SetCursorPosition(0, consoleTop);
                        FileManipulationPromt(fileManager);
                        consoleTop = Console.CursorTop;
                        clearConsole(0, consoleTop);
                        break;
                    default:
                        Console.WriteLine(); // To not write over existing line.
                        Environment.Exit(1);
                        break;
                }
                // To not remove the content.
                consoleTop = Console.CursorTop - 1;
                Console.ForegroundColor = ConsoleColor.White;
            }
        }
        // The promt used for the dracula manipulation.
        static void FileManipulationPromt(FileManagerService fileManager)
        {
            int consoleTop = Console.CursorTop;
            bool active = true;
            int selectedOption = 3;
            // Main loop
            while (active)
            {
                Console.Write(
                    $"Please select one of the options\n" +
                    $"{(selectedOption == 3 ? '>' : (' '))}  Name of the file\n" +
                    $"{(selectedOption == 2 ? '>' : (' '))}  How big the file is\n" +
                    $"{(selectedOption == 1 ? '>' : (' '))}  How many lines the file has\n" +
                    $"{(selectedOption == 0 ? '>' : (' '))}  Search for a word\n\n" +
                    $"Press escape to go back"
                    );
                consoleTop = Console.CursorTop - 6;

                // Listening for user input and acting accordingly
                ConsoleKeyInfo userInput = Console.ReadKey();
                while (userInput.Key != ConsoleKey.Enter || selectedOption == -1)
                {
                    switch (userInput.Key)
                    {
                        case ConsoleKey.UpArrow:
                            if (selectedOption >= 0 && selectedOption < 3)
                            {
                                selectedOption++;
                            }
                            break;
                        case ConsoleKey.DownArrow:
                            if (selectedOption > 0 && selectedOption <= 3)
                            {
                                selectedOption--;
                            }
                            break;
                        case ConsoleKey.Escape:
                            selectedOption = -1;
                            break;
                        default:
                            break;
                    }
                    if (selectedOption == -1)
                    {
                        break;
                    }

                    clearConsole(0, consoleTop);
                    Console.Write(
                                $"Please select one of the options\n" +
                                $"{(selectedOption == 3 ? '>' : (' '))}  Name of the file\n" +
                                $"{(selectedOption == 2 ? '>' : (' '))}  How big the file is\n" +
                                $"{(selectedOption == 1 ? '>' : (' '))}  How many lines the file has\n" +
                                $"{(selectedOption == 0 ? '>' : (' '))}  Search for a word\n"
                        );
                    consoleTop = Console.CursorTop - 5;
                    userInput = Console.ReadKey();
                }

                // If the user pressed escape
                if (selectedOption == -1)
                {
                    clearConsole(0, consoleTop);
                    Console.CursorTop = Console.CursorTop + 1;
                    break;
                }

                Console.ForegroundColor = ConsoleColor.Cyan;
                clearConsole(0, consoleTop);
                // Invoke function based on the selected option.
                switch (selectedOption)
                {
                    case 3:
                        Console.WriteLine($"The filename is {fileManager.FileName()}");
                        break;
                    case 2:
                        Console.WriteLine($"The filesize is {fileManager.FileSize()} bytes");
                        break;
                    case 1:
                        Console.WriteLine($"There are {fileManager.FileLineLength()} in the file");
                        break;
                    case 0:
                        Console.WriteLine("Type the word to search for.");
                        Console.CursorVisible = true;
                        string word = Console.ReadLine();

                        // Removes the user input
                        Console.SetCursorPosition(0, Console.CursorTop - 1);
                        Console.Write(new string(' ', Console.WindowWidth));
                        Console.SetCursorPosition(0, Console.CursorTop - 1);

                        Console.CursorVisible = false;
                        (string, int) wordSearch = fileManager.FindAndCountWord(word);
                        Console.WriteLine($"The word {wordSearch.Item1} was found {wordSearch.Item2} times.");
                        break;
                    default:
                        Console.WriteLine("Not a valid option");
                        break;
                }
                consoleTop = Console.CursorTop;
                Console.ForegroundColor = ConsoleColor.White;
            }
        }
        // Removes all content from the left top position. 
        // And puts the console cursor back to it's original position
        static void clearConsole(int left, int top)
        {
            Console.SetCursorPosition(left, top);
            for (int i = top; i < Console.WindowHeight; i++)
            {
                Console.SetCursorPosition(0, i);
                Console.Write(new string(' ', Console.WindowWidth));
            }

            Console.SetCursorPosition(left, top);
        }

        static void fileExtensionPromt(FileManagerService fileManager)
        {
            string[] fileExts = fileManager.ListFileTypes();
            int selectedOption = fileExts.Length - 1;
            int consoleTop = Console.CursorTop;

            clearConsole(0, consoleTop);
            Console.WriteLine("Available extension to choose from");
            for (int i = 0; i < fileExts.Length; i++)
            {
                Console.WriteLine($"{(selectedOption == i ? '>' : (' '))} {fileExts[i]}");
            }

            consoleTop = Console.CursorTop - fileExts.Length - 1;

            ConsoleKeyInfo userInput = Console.ReadKey();
            while (userInput.Key != ConsoleKey.Enter)
            {
                switch (userInput.Key)
                {
                    case ConsoleKey.DownArrow:
                        if (selectedOption >= 0 && selectedOption < fileExts.Length - 1)
                        {
                            selectedOption++;
                        }
                        break;
                    case ConsoleKey.UpArrow:
                        if (selectedOption > 0 && selectedOption <= fileExts.Length - 1)
                        {
                            selectedOption--;
                        }
                        break;
                    default:
                        break;
                }
                clearConsole(0, consoleTop);
                Console.WriteLine("Available extension to choose from");
                for (int i = 0; i < fileExts.Length; i++)
                {
                    Console.WriteLine($"{(selectedOption == i ? '>' : (' '))} {fileExts[i]}");
                }
                consoleTop = Console.CursorTop - fileExts.Length - 1;
                userInput = Console.ReadKey();
            }

            clearConsole(0, consoleTop);

            string[] files = fileManager.getFilesByExt(fileExts[selectedOption]);
            Console.WriteLine($"Files Containing the {fileExts[selectedOption]} extension");
            foreach (string file in files)
            {
                Console.WriteLine(file);
            }
            Console.WriteLine();
        }
    }
}
