# Filesystem Manager 1000

## Features:

- List all the files in the resource folder
- List all the file extensions for the files in the folder
- Manipulate the dracula.txt file
  - Display the file name
  - Display the file size
  - Display how many lines the file has
  - Search for a word
      - Word match must be exact:
        - "dracula" != dracula
        - it's != its etc..
- Automatically create a log folder and file
  - logs all the action the program does.

## Using the program:

- A resource folder with a text file named 'dracula.txt' is required
  - Need to be placed in the root folder
- Use the arrow keys to select and enter to confirm
