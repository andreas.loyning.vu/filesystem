﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FilesystemManager.Enums
{
    public enum FileOperations
    {
        LIST_FILES,
        LIST_FILE_EXT,
        FILE_NAME,
        FILE_SIZE,
        COUNT_LINES,
        FIND_WORD,
        GET_FILE_BY_EXT,
    }
}
